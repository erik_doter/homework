#include <gtest/gtest.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "numbers.h"


int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}


TEST(find_numbers, Testing1) {
	char **s;
	s = (char**)malloc(2*sizeof(char*));
	s[0] = (char*)malloc(15*sizeof(char));
	s[1] = (char*)malloc(15*sizeof(char));
    snprintf(s[0], sizeof(s[0]), "+79250304528");
	snprintf(s[1], sizeof(s[1]), "qwqwqwqwqwqw");
	result res;
	int line = 2;
	int answer = find_numbers(s, line, &res);
	free(s[1]);
	free(s[2]);
	free(s);
	ASSERT_EQ(1, answer);
}

TEST(find_numbers, emptystr) {
	char **s;
	s = (char**)malloc(3*sizeof(char*));
	s[0] = (char*)malloc(2*sizeof(char));
	s[1] = (char*)malloc(2*sizeof(char));
	s[2] = (char*)malloc(2*sizeof(char));
    snprintf(s[0], sizeof(s[0]), "\n");
	snprintf(s[1], sizeof(s[1]), "\n");
	snprintf(s[2], sizeof(s[2]), "\n");
	result res;
	int line = 3;
	int answer = find_numbers(s, line, &res);
	free(s[0]);
	free(s[1]);
	free(s[2]);
	free(s);
	ASSERT_EQ(0, answer);
}

TEST(find_numbers, nonumbers) {
	char **s;
	s = (char**)malloc(3*sizeof(char*));
	s[0] = (char*)malloc(20*sizeof(char));
	s[1] = (char*)malloc(20*sizeof(char));
	s[2] = (char*)malloc(20*sizeof(char));
    snprintf(s[0], sizeof(s[0]), "My name is Erik");
	snprintf(s[1], sizeof(s[1]), "Hello World!");
	snprintf(s[2], sizeof(s[2]), "QWERTYKSTR");
	result res;
	int line = 3;
	int answer = find_numbers(s, line, &res);
	free(s[0]);
	free(s[1]);
	free(s[2]);
	free(s);
	ASSERT_EQ(0, answer);
}

TEST(find_numbers, allnumbers) {
	char **s;
	s = (char**)malloc(3*sizeof(char*));
	s[0] = (char*)malloc(25*sizeof(char));
	s[1] = (char*)malloc(25*sizeof(char));
	s[2] = (char*)malloc(25*sizeof(char));
    snprintf(s[0], sizeof(s[0]), "+79250304528 hello");
	snprintf(s[1], sizeof(s[1]), "tel:+79348389293");
	snprintf(s[2], sizeof(s[2]), "+79839290482");
	result res;
	int line = 3;
	int answer = find_numbers(s, line, &res);
	free(s[0]);
	free(s[1]);
	free(s[2]);
	free(s);
	ASSERT_EQ(3, answer);
}

TEST(find_numbers, NumbersInOneLine) {
	char **s;
	s = (char**)malloc(3*sizeof(char*));
	s[0] = (char*)malloc(50*sizeof(char));
	s[1] = (char*)malloc(50*sizeof(char));
	s[2] = (char*)malloc(50*sizeof(char));
    snprintf(s[0], sizeof(s[0]), "first number +79250304528 second +79347583838");
	snprintf(s[1], sizeof(s[1]), "don't forget to call me");
	snprintf(s[2], sizeof(s[2]), "Please, don't forget");
	result res;
	int line = 3;
	int answer = find_numbers(s, line, &res);
	free(s[0]);
	free(s[1]);
	free(s[2]);
	free(s);
	ASSERT_EQ(1, answer);
}