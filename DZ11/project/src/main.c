#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "numbers.h"

int main() {
    int answer = 0, line = 0;
    char **str;
    int i = 0;
    result res;
    printf("введите количество строк: ");
    scanf("%d", &line);
    str = read_custom(line);
    answer = find_numbers(str, line, &res);
    if (answer == -1) {
        fprintf(stderr, "Error\n");
    }
    printf("Найдено %d\n", answer);
    for (i = 0; i < answer; i++) {
        printf("%s", res.res_str[i]);
        free(res.res_str[i]);
    }
    free(res.res_str);
    return 0;
}
