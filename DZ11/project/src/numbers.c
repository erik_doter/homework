#include "numbers.h"
#define BUFF_SIZE 64

char** read_custom(int num_of_lines) {
    char **s;
    char **t = malloc(sizeof(char*));
    if (t == NULL) {
        return 0;
    } else {
        s = t;
    }
    int pos = 1, len = 0, num = 0, line = 0;
    s[line] = malloc(BUFF_SIZE);
    if (s[line] == NULL) {
        return NULL;
    }
    while (line <= num_of_lines) {
        char c;
        c = getchar();
        len++;
        if (len >= BUFF_SIZE) {
            s[line] = realloc(s[line], BUFF_SIZE * pos);  //Если превышен размер буфера, то расширять
            if (s == NULL) {
                return NULL;
            }
            len = 0;
            pos++;
        }
        s[line][num] = c;
        num++;
        if (c == '\n') {
            s[line][num] = '\0';
            pos = 0;
            len = 0;
            num = 0;
            line++;
            char **p = realloc(s, (line+1) * sizeof(char*));
            if (p == NULL) {
                return NULL;
            } else {
                s = p;
            }
            s[line] = malloc(BUFF_SIZE);
            if (s[line] == NULL) {
                return NULL;
            }
        }
    }
    return s;
}

int find_numbers(char** s, int line, result *res) {
    int i = 0, j = 0;
    int num_of_num = 0;    // количество подряд идущих цифр
    int flag = 0;          // true или false, чтобы понимать, идут ли цифры подряд
    res->lines = 0;
    if (s == NULL) {
        return -1;
    }
    res->res_str = malloc(sizeof(char*));
    if (res->res_str == NULL) {
        return -1;
    }
    for (i = 0; i <= line; i++) {
        while (s[i][j] != '\0') {                        // проход по всем символам массива строк
            if (s[i][j] == '+' || isdigit(s[i][j])) {
                flag = 1;
            } else {
                flag = 0;
            }
            switch (flag) {
                case 0: num_of_num = 0;
                break;
                case 1: num_of_num++;
                break;
            }
            if (num_of_num > 1 && s[i][j] == '+') {
                num_of_num--;
            }
            if (num_of_num == 1 && isdigit(s[i][j])) {
                num_of_num--;
                flag = 0;
            }
            if (num_of_num == 12 && !(isdigit(s[i][j+1]))) {
                res->res_str = realloc(res->res_str, (res->lines + 2) * sizeof(char*));
                if (res->res_str == NULL) {
                    return -1;
                }
                res->res_str[res->lines] = malloc(strlen(s[i]) * sizeof(char));
                if (res->res_str[res->lines] == NULL) {
                    return -1;
                }
                for (int n = 0; s[i][n] != '\0'; n++) {
                    res->res_str[res->lines][n] = s[i][n];
                }
                res->lines++;
                num_of_num = 0;
                break;
            }
            j++;
        }
        j = 0;
    }
    for (int i = 0; i < line; i++) {
        free(s[i]);
    }
    free(s);
    return res->lines;
}
