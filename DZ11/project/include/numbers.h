#ifndef HOMEWORK_DZ11_PROJECT_INCLUDE_NUMBERS_H_
#define HOMEWORK_DZ11_PROJECT_INCLUDE_NUMBERS_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>


typedef struct result {
    char** res_str;
    int lines;
} result;


int find_numbers(char** str, int line, result *res);
char** read_custom(int num);


#endif /* HOMEWORK_DZ11_PROJECT_INCLUDE_NUMBERS_H_ */

